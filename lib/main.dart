import 'package:bloc_form_validation/bloc/provider.dart';
import 'package:bloc_form_validation/pages/HomePage.dart';
import 'package:bloc_form_validation/pages/LoginPage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Bloc Form',
        theme: ThemeData(
          primaryColor: Colors.deepPurple,
        ),
        initialRoute: LoginPage.routeName,
        routes: {
          HomePage.routeName: (_) => HomePage(),
          LoginPage.routeName: (_) => LoginPage(),
        },
      ),
    );
  }
}
