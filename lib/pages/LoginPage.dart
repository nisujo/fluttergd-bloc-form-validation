import 'package:bloc_form_validation/bloc/login_bloc.dart';
import 'package:bloc_form_validation/bloc/provider.dart';
import 'package:bloc_form_validation/pages/HomePage.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  static final String routeName = 'login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          background(context),
          Positioned(left: 10.0, top: 40.0, child: bubble()),
          Positioned(top: -20.0, right: -20.0, child: bubble()),
          Center(
            child: Container(
              child: SafeArea(
                child: SingleChildScrollView(
                  child: mainContent(context),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget bubble() {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05),
      ),
    );
  }

  Widget background(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Container(
          height: size.height * 0.4,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(63, 63, 156, 1.0),
                Color.fromRGBO(90, 70, 178, 1.0),
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            height: size.height * 0.6,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  Widget mainContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      width: MediaQuery.of(context).size.width * 0.95,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.location_pin,
            color: Colors.white,
            size: 50.0,
          ),
          SizedBox(height: 10.0),
          Text(
            'Jose Nieto',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22.0,
              color: Colors.white,
            ),
          ),
          form(context),
          Text('¿Olvidó la contraseña?'),
        ],
      ),
    );
  }

  Widget form(BuildContext context) {
    final bloc = CustomProvider.of(context);

    return Card(
      elevation: 5.0,
      margin: EdgeInsets.symmetric(
        vertical: 25.0,
        horizontal: 10.0,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 30.0,
          horizontal: 20.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Ingreso',
              style: TextStyle(fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 30.0),
            emailField(bloc),
            SizedBox(height: 10.0),
            passwordField(bloc),
            SizedBox(height: 30.0),
            submitButton(bloc),
          ],
        ),
      ),
    );
  }

  Widget emailField(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return TextFormField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            labelText: 'Correo electrónico',
            hintText: 'user@mail.com',
            icon: Icon(Icons.alternate_email),
            counterText: snapshot.data,
            errorText: snapshot.error,
          ),
          onChanged: (String value) => bloc.changeEmail(value),
        );
      },
    );
  }

  Widget passwordField(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return TextFormField(
          obscureText: true,
          decoration: InputDecoration(
            labelText: 'Contraseña',
            icon: Icon(Icons.lock_outline),
            counterText: snapshot.data,
            errorText: snapshot.error,
          ),
          onChanged: (String value) => bloc.changePassword(value),
        );
      },
    );
  }

  Widget submitButton(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: RaisedButton(
            child: Text('Ingresar'),
            elevation: 0,
            color: Colors.deepPurple,
            textColor: Colors.white,
            padding: EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 20.0,
            ),
            onPressed:
                snapshot.hasData ? () => _makeLogin(context, bloc) : null,
          ),
        );
      },
    );
  }

  void _makeLogin(BuildContext context, LoginBloc bloc) {
    print('email: ${bloc.email}');
    print('password: ${bloc.password}');
    Navigator.of(context).pushReplacementNamed(HomePage.routeName);
  }
}
