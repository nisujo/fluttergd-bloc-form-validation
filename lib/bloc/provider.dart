import 'package:bloc_form_validation/bloc/login_bloc.dart';
import 'package:flutter/material.dart';

class CustomProvider extends InheritedWidget {
  final loginBloc = LoginBloc();

  static CustomProvider _instance;

  factory CustomProvider({Key key, Widget child}) {
    if (_instance == null) {
      _instance = CustomProvider._internal(key: key, child: child);
    }

    return _instance;
  }

  CustomProvider._internal({Key key, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }

  static LoginBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CustomProvider>()
        .loginBloc;
  }
}
